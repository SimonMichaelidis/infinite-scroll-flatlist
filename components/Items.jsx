//import React in our code
import React from 'react';
import { View, Text, Image, StyleSheet, Linking } from 'react-native';

const openURL = (url) => {
  
  Linking.openURL(url).catch((err) => console.error('An error occurred', err));
}
//  Fake Date
const date = new Date()

// Ternaire (post ou ggnews)
export const renderItem = ({ item }) => (
    item.type == 'post' ? ( 
      <View style={styles.itemStyle}>
        <View style={styles.post}>
          { item.image ? ( 
          <Image style={ styles.avatar } source={{uri: item.image }}/> 
          ) : (
          <Image source={require('../assets/avatar.jpg')} style = {styles.avatar} />
          )
          }
          <View style={styles.main}>
            <View style={styles.headerGgnews}>
              <Text style={styles.name}> { item.lastname } { item.firstname } </Text>
              <Text> a publié un statut </Text>
            </View>
            <Text> Le { item.created } </Text> 
          </View>
        </View>
      </View>
    ):(
      <View style={styles.itemStyle}>
        <View style={styles.ggnews}>
        { item.image ? ( 
        <Image style={ styles.image } source={{uri: item.image}}/>
        ) : (null)} 
        <Text style={styles.date}> Le { item.created }</Text>
        <Text style={styles.title} onPress={() => openURL(item.link)}> { item.content } </Text>
        </View>
      </View>
  )
  );

const styles = StyleSheet.create({
  itemStyle: { 
    width: '100vw',
    fontSize: 4,
    display: 'flex',
    flexDirection: 'column',
    boxShadow: '3px 3px 5px gray',
    padding: 10,
    margin: 10, // On peut gérer les margins directement depuis la Flatlist (ItemSeparatorComponent#)    )
  },
  ggnews: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-start',
  },
  headerGgnews:{
    display: 'flex',
    flexDirection: 'row'
  },
  post: {
    display: 'flex',
    flexDirection: 'row',    
  }, 
  main: {
    display: 'flex',
    flexDirection: 'column',
  },
  avatar: {
    height: 105, 
    width: 105, 
    resizeMode : 'stretch'
  },
  image : {
    height: '40vh',
    // resizeMode : '',
  },
  title: {
    fontWeight: 'bold',
  },
  name: {
    fontWeight: 'bold',
  },
  date: {
    fontSize: 14,
    marginBottom: 10, 
    marginTop: 10, 
  }
});