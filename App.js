// Infinite Scroll FlatList
// API : https://www.sportyma.com/api/news

// Supports :
// https://medium.com/@srbkrishnan/infinite-scroll-pagination-in-flatlist-with-hooks-and-function-components-c9c08bba23a8
// https://scotch.io/tutorials/implementing-an-infinite-scroll-list-in-react-native

//Notes : 
// On peut choisir la solution du custom Hook (ex:useFetch) pour fetch les données 
// ce qui nous permettrait d'avoir une page "App" épurée. 

// Ajouts : 
// - un loader afin de rendre l'effet plus fluide 
// - la fonctionalité pull to refresh
// - du style

// Fix : 
// Solve React useEffect Memory Leak Cancel Subscriptions Cleanup Function
// https://medium.com/@selvaganesh93/how-to-clean-up-subscriptions-in-react-components-using-abortcontroller-72335f19b6f7


import React, {useState, useEffect} from 'react';
import { SafeAreaView, StyleSheet, FlatList, Text } from 'react-native';
import  { renderItem }  from './components/Items'

const App = () => {

  const [news, setNews] = useState([]);
  const [page, setPage] = useState(0)

  // Remplace componentDidMount de l'approche class
  useEffect(() => getAllNews(), []);

  // Fetch all from API
  //!! Problème avec le 'mode : no-cors / cors' lors de l'appel API; Installation du plugin Moesif Cors sur Browser
  const getAllNews = () => {
    const URL = `https://www.sportyma.com/api/news?page=${page}`;
    const requestOptions = {
      method: 'POST',
      header: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        'Access-Control-Allow-Credentials': 'true'
      },
      redirect: 'follow'
    };

    // Fetch : Si première page (0) prends le résultat - 1er montage -, sinon j'ajoute aux résultats précédents 

    fetch(URL, requestOptions)
      .then(response => response.json())
      .then(result => setNews(prevState => page === 0 ? result.items : [...prevState, ...result.items]))
      .catch(error => console.log('error', error)); 
  };

  const getMoreNews = () => {
    setPage(prevState => prevState + 1)
    getAllNews()
  };
  
  // console.log(news)
  return (
    <SafeAreaView style={styles.container}>
      <Text style={styles.mainTitle}> Infinite Scroll - FlatList </Text>
        <FlatList
          data={news}
          keyExtractor={(item, index) => index.toString()} 
          renderItem={renderItem}
          onEndReached={getMoreNews} 
          onEndReachedThreshold={0.4}
        />
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  container: {
    width: '100vw',
    height: '100vh', // require (onEndReached dw)
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: '#F6F6F6'
  },
  mainTitle:{
    textAlign: 'center',
    fontSize: 25,
    margin: 5,
  }
});

export default App;